#!/usr/bin/env sh

trap "exit 1" TERM
TOP_PID=$$

KUBECTL_ARGS=""
LOOP_WAIT_TIME="${WAIT_TIME:-2}" # seconds
JOB_WAIT_TIME=5
DEBUG="${DEBUG:-0}"
TREAT_ERRORS_AS_READY=2
JOB_STATE="0:0:0"

debug() {
    LEVEL=$1
    MESSAGE=$2
    if [[ $LEVEL -eq $DEBUG || $LEVEL -lt $DEBUG ]]; then
        echo "[$(date +'%Y-%m-%d %H:%M:%S')] ## $MESSAGE" >&2
        #printf "[%s] %s %s%s is ready.\\n" "$(date +'%Y-%m-%d %H:%M:%S')" "$1" "$2" "$print_KUBECTL_ARGS"
    fi
}

configTemplate() {
    get_job_state_name=$1 
cat  <<EOF > /tmp/configMap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: $get_job_state_name
data:
  usedimage: unknow
  lastpodstatus: "$JOB_STATE"
EOF
}

usage() {
cat <<EOF
This script waits until a job, pod or service enter ready state. 

${0##*/} job [<job name> <image name>]

Examples:
Wait for all pods with a following label to enter 'Ready' state:
${0##*/} pod -lapp=develop-volume-gluster-krakow
Wait for all selected pods to enter the 'Ready' state:
${0##*/} pod -l"release in (develop), chart notin (cross-support-job-3p)"

EOF
exit 1
}

### configMap functions
createConfigMap_jobname() {
    local JOBNAME=$1
    
    debug 0 "apply configMap Template"
    configTemplate $JOBNAME
    createConfigMap=$(kubectl apply -f /tmp/configMap.yaml 2>&1)
    local state=$? 
    
    #RETURN
    echo $createConfigMap
    return $state
}

getConfigMapManifest_jobname_param() {
    local JOBNAME=$1
    local PARAM="$2"
    local KCTL="${3:-get}"

    if [ $KCTL = "describe" ]; then
        PARAM=""
    fi

    getConfigMap=$(kubectl $KCTL configMaps "$JOBNAME" $PARAM 2>&1)
    local state=$?

    debug 2 "##### getConfigMapImage_jobname"
    debug 2 "$getConfigMap"
    debug 2 "$state"

    #RETURN
    echo $getConfigMap
    return $state
}

getConfigMapImage_jobname() {
    local JOBNAME="$1"

    getConfigImage=$(kubectl get configMap "$JOBNAME" -o yaml)
    local state=$?
    getConfigImage=$(kubectl get configMap "$JOBNAME" -o yaml | awk '/usedimage:/{print $NF}')

    debug 2 "##### getConfigMapImage_jobname"
    debug 2 "$getConfigImage"
    debug 2 "$state"

    #RETURN
    echo $getConfigImage
    return $state
}

getConfigMapStatus_jobname() {
    local JOBNAME="$1"

    getConfigStatus=$(kubectl get configMap "$JOBNAME" -o yaml)
    local state=$?
    getConfigStatus=$(kubectl get configMap "$JOBNAME" -o yaml | awk '/lastpodstatus:/ {print $NF}' | tr -d '"')

    debug 2 "getConfigMapStatus_jobname"
    debug 2 "$getConfigStatus"
    debug 2 "$state"
   
    #RETURN
    echo $getConfigStatus
    return $state
}

patchConfigMap_jobname_key_value() {
    local JOBNAME=$1
    local KEY=$2
    local VALUE=$3

    if [ $VALUE != "" ]; then
        #podstatus
        debug 0 "patch $KEY:$VALUE in configMap"
        patch="{\"data\":{\"$KEY\":\"$VALUE\"}}"
        patchConfigMap=$(kubectl patch configMap "$JOBNAME" -p $patch 2>&1) 
        local state=$?
    else
        debug 0 "$KEY has no value."
    fi


    #RETURN
    echo $patchConfigMap
    return $state
}

### Job Functions
getJobManifest_jobname_param() {
    local JOBNAME="$1"
    local PARAM="$2"
    local KCTL="${3:-get}"

    if [ $KCTL = "describe" ]; then
        PARAM=""
    fi

    getJobStatus=$(kubectl $KCTL jobs "$JOBNAME" $PARAM 2>&1) 
    local state=$?
    
    #RETURN
    echo $getJobStatus
    return $state
}

getJobImage_jobname() {
    local JOBNAME="$1"

    getJobImage=$(kubectl get jobs "$JOBNAME" -o yaml 2>&1)
    local state=$?
    getJobImage=""

    if [ $state -eq 0 ]; then
        getJobImage=$(kubectl get jobs "$JOBNAME" -o yaml 2>/dev/null | awk '/image:/{print $NF}')
    fi

    debug 2 "getJobImage_jobname"
    debug 2 "$getJobImage"
    debug 2 "$state"   

    #RETURN
    echo $getJobImage
    return $state
}

getJobState_LastJobManifestOutput() {
    local MANIFESTOUTPUT=$1
    local get_job_state_output=""

    get_job_state_output=$(printf "%s" "$MANIFESTOUTPUT" | sed -nr 's#.*:[[:blank:]]+([[:digit:]]+) [[:alpha:]]+ / ([[:digit:]]+) [[:alpha:]]+ / ([[:digit:]]+) [[:alpha:]]+.*#\1:\2:\3#p' 2>&1)
    local state=$?
    if [ "$get_job_state_output" == "" ] || echo "$get_job_state_output" | grep -q "No resources found"; then
        debug 0 "No jobs found!" >&2
        return 1
    else
        #RETURN
        echo $get_job_state_output
        return $state
    fi
}

# Other Functions
isJobImgAndConfigImgEqual_jobname() {
    local JOBNAME="$1"

    resultConfigMapImage=$(getConfigMapImage_jobname $JOBNAME)
    resultConfigMapImage_state=$?
    resultJobImage=$(getJobImage_jobname $JOBNAME)
    resultJobImage_state=$?

    debug 2 "isJobImgAndConfigImgEqual_jobname"
    debug 2 "config vs job"
    debug 2 "$resultConfigMapImage - $resultJobImage"
    debug 2 "$resultConfigMapImage_state - $resultJobImage_state"

    if [ $resultJobImage_state -ne 0 ]; then
        return 2
    elif [ $resultConfigMapImage_state -ne 0 ]; then
        return 2
    elif ! [ "$resultConfigMapImage" == "$resultJobImage" ]; then
        return 1
    else
        echo "$resultJobImage"
        return 0
    fi
}

isJobStatusReady_lastJobStateOutput() {
    local LASTJOBSTATUSOUTPUT="$1"

        # Map triplets of <running>:<succeeded>:<failed> to not ready (emit 1) state
    if [ $TREAT_ERRORS_AS_READY -eq 0 ]; then
        # Two conditions: 
        #   - pods are distributed between all 3 states with at least 1 pod running - then emit 1
        #   - or more then 1 pod have failed and some are completed - also emit 1
        sed_reg='-e s/^[1-9][[:digit:]]*:[[:digit:]]+:[[:digit:]]+$/1/p -e s/^0:[[:digit:]]+:[1-9][[:digit:]]*$/1/p'
    elif [ $TREAT_ERRORS_AS_READY -eq 1 ]; then
        # When allowing for failed jobs
        #   - pods are distributed between all 3 states with at least 1 pod running- then emit 1
        #   - all other options include all pods Completed or Failed - which are fine
        sed_reg='-e s/^[1-9][[:digit:]]*:[[:digit:]]+:[[:digit:]]+$/1/p'
    elif [ $TREAT_ERRORS_AS_READY -eq 2 ]; then
        # When allowing for failed jobs but at least one pod have to Succeed
        #   - pods are distributed between all 3 states with at least 1 pod running- then emit 1
        #   - some pods are failed, but no pod is completed yet - then emit 1
        #   - when no pod is running and at least one is completed - all is fine
        sed_reg='-e s/^[1-9][[:digit:]]*:[[:digit:]]+:[[:digit:]]+$/1/p -e s/^0:0:[[:digit:]]+$/1/p'
    fi 
    get_job_state_output2=$(printf "%s" "$LASTJOBSTATUSOUTPUT" | sed -nr $sed_reg )
    local state1=$?
    debug 9 "printf \"%s\" \"$LASTJOBSTATUSOUTPUT\" | sed -nr $sed_reg"
    checkJobStatus=$(printf "%s" "$get_job_state_output2" | xargs )
    local state=$?
    debug 9 "LAST $LASTJOBSTATUSOUTPUT"
    debug 9 "OUT2 $get_job_state_output2"
    debug 9 "CHEC $checkJobStatus"
    debug 9 "CODE $state1"
    debug 9 "CODE $state"

    #RETURN
    echo $checkJobStatus
    return $state
}

# This will check jobs and configs
isJobOrConfRdy_jobname() {
    local JOBNAME="$1"
    local JOBSTATE=""
    JOB_MANIFEST=$(getJobManifest_jobname_param "$JOBNAME" "" "describe")
    JOB_MANIFEST_CODE=$?
    debug 2 "JOB STATE $JOBNAME $JOB_MANIFEST_CODE"
    if [ $JOB_MANIFEST_CODE -ne 0 ]; then
        # JOB doesn't exist
        debug 2 "JOB doesn't exist"
        CONF_MANIFEST=$(getConfigMapManifest_jobname_param "$JOBNAME" "-o yaml")
        CONF_MANIFEST_CODE=$?
        if [ $CONF_MANIFEST_CODE -ne 0 ]; then
            # configMap doesn't exist
            debug 0 "No Job and no configMap exist with name $JOBNAME"
            kill -s TERM $TOP_PID
        else
            # configMap exist, get Job status
            debug 0 "Configmap exists but no Job is here. Wait $JOB_WAIT_TIME seconds for Job to start"
            sleep $JOB_WAIT_TIME
            JOB_EQ_IMG=$(isJobImgAndConfigImgEqual_jobname "$JOBNAME")
            JOB_EQ_IMG_CODE=$?
            JOB_MANIFEST=$(getJobManifest_jobname_param "$JOBNAME" "" "describe")
            JOB_STATE=$(getJobState_LastJobManifestOutput "$JOB_MANIFEST")
            if [ $? -ne 0 ]; then
                JOB_STATE=$(getConfigMapStatus_jobname $JOBNAME)
            fi
            JOB_GET_IMAGE=$(getJobImage_jobname "$JOBNAME")
            if [ $JOB_EQ_IMG_CODE -eq 0 ]; then
                # Job and ConfigMap image are equal.
                debug 0 "Job found and images are identical: Patch laststate Run:Rdy:Fail $JOB_STATE"
                PATCHCONFSTATE=$(patchConfigMap_jobname_key_value "$JOBNAME" "lastpodstatus" "$JOB_STATE")
            elif [ $JOB_EQ_IMG_CODE -eq 2 ]; then
                CONFIGMAP_STATUS=$(getConfigMapStatus_jobname $JOBNAME)
                CONFIGMAP_STATUS_CODE=$?
                ## Configmap doesn't exist. This is the first run.
                if [ $CONFIGMAP_STATUS_CODE -ne 0 ]; then
                    debug 0 "configMap Missing"
                else
                    debug 0 "Job not found and configMap has state $CONFIGMAP_STATUS Run:Rdy:Fail."
                fi
            else
                # Job found. But images are not equal.
                debug 0 "image in Job and configMap are not equal, patch configMap"
                PATCHCONF=$(patchConfigMap_jobname_key_value "$JOBNAME" "lastpodstatus" "$JOB_STATE")
                PATCHCONFIMG=$(patchConfigMap_jobname_key_value "$JOBNAME" "usedimage" "$JOB_GET_IMAGE")
            fi

        fi
    elif [ $JOB_MANIFEST_CODE -eq 0 ]; then
        # JOB exist
        debug 2 "JOB exist"
        JOB_STATE=$(getJobState_LastJobManifestOutput "$JOB_MANIFEST")
        CONF_MANIFEST=$(getConfigMapManifest_jobname_param "$JOBNAME")
        CONF_MANIFEST_CODE=$?
        if [ $CONF_MANIFEST_CODE -ne 0 ]; then
            debug 0 "Config doesn't exist, create one"
            CONF_CREATE=$(createConfigMap_jobname "$JOBNAME")
        fi
        debug 2 "Patch configMap"
        JOB_GET_IMAGE=$(getJobImage_jobname "$JOBNAME")
        PATCHCONFIMG=$(patchConfigMap_jobname_key_value "$JOBNAME" "usedimage" "$JOB_GET_IMAGE")
        PATCHCONFSTATE=$(patchConfigMap_jobname_key_value "$JOBNAME" "lastpodstatus" "$JOB_STATE")
        PATCHCONFSTATE_CODE=$?
        if [ $PATCHCONFSTATE_CODE -ne 0 ]; then
            JOB_STATE="1:0:0"
        fi
    fi

    result=$(isJobStatusReady_lastJobStateOutput "$JOB_STATE")
    local state=$?
    echo $result
    return $state
}

wait_for_resource() {
    wait_for_resource_type=$1
    wait_for_resource_descriptor="$2"
    #while [ -n "$(get_${wait_for_resource_type}_state "$wait_for_resource_descriptor")" ] ; do
    while [ -n "$(isJobOrConfRdy_jobname "$wait_for_resource_descriptor")" ] ; do
        print_KUBECTL_ARGS="$KUBECTL_ARGS"
        [ "$print_KUBECTL_ARGS" != "" ] && print_KUBECTL_ARGS=" $print_KUBECTL_ARGS"
        echo "Waiting for $wait_for_resource_type $wait_for_resource_descriptor${print_KUBECTL_ARGS}..."
        sleep "$LOOP_WAIT_TIME"
    done
    ready "$wait_for_resource_type" "$wait_for_resource_descriptor"
}

ready() {
    print_KUBECTL_ARGS="$KUBECTL_ARGS"
    [ "$print_KUBECTL_ARGS" != "" ] && print_KUBECTL_ARGS=" $print_KUBECTL_ARGS"
    printf "[%s] %s %s%s is ready.\\n" "$(date +'%Y-%m-%d %H:%M:%S')" "$1" "$2" "$print_KUBECTL_ARGS"
}

main() {
    if [ $# -lt 2 ]; then
        usage
    fi

    main_resource=$1
    shift
    main_name="$1"
    shift
    DEBUG="${1:-0}"
    shift
    KUBECTL_ARGS="${*}"
    echo "start script with $0 $main_resource $main_name $DEBUG $KUBECTL_ARGS"
    wait_for_resource "$main_resource" "$main_name"
    exit 0
}

main "$@"