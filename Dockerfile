# alpine:3.12.1
FROM alpine@sha256:c0e9560cda118f9ec63ddefb4a173a2b2a0347082d7dff7dc14272e7841a5b5a

ARG TARGETPLATFORM
ARG TARGETOS
ARG TARGETARCH

ENV KUBE_VERSION="1.24.0"

# RUN apk add --update --no-cache ca-certificates=20191127-r4 curl=7.79.1-r0 jq=1.6-r1
RUN apk add --update --no-cache ca-certificates=20220614-r0 curl=7.79.1-r1 jq=1.6-r1
RUN curl -L https://storage.googleapis.com/kubernetes-release/release/v${KUBE_VERSION}/bin/$TARGETOS/$TARGETARCH/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ADD ohmyhelm-job-helper.sh /usr/local/bin/ohmyhelm-job-helper.sh

ENTRYPOINT ["ohmyhelm-job-helper.sh"]